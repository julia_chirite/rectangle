import java.util.Scanner;

public class Rectangle {
	public static void main(String [] args) {
		Scanner keyboard = new Scanner (System.in);
		Double height;
		Double width;
		System.out.println("Enter rectangle width");
		width = keyboard.nextDouble();

		System.out.println("Enter rectangle height");
		height = keyboard.nextDouble();

		System.out.println("The perimeter of the rectangle is " + 2*(width + height));
		System.out.println("The area of the rectangle is " + width * height);	

	}
}
